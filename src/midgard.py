from util import HTTPClient, isMainnet

class Midgard(HTTPClient):
    def __init__(self, base_url=None):
        if base_url is None:
            if isMainnet():
                self.base_url = "https://midgard.thorchain.info"
            else:
                self.base_url = "https://testnet.midgard.thorchain.info"
        else:
            self.base_url = base_url

    def health(self):
        try:
            self.get("/v2/health", {"timeout": 1})
            return True
        except:
            return False

    def get_pool(self, asset):
        return self.fetch(f"/v2/pool/{asset}")
