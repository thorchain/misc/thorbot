from util import HTTPClient

class CoinGecko(HTTPClient):
    def __init__(self):
        self.base_url = "https://api.coingecko.com/api/v3"
        self.coins = self.get_coins()

    def get_coins(self):
        return self.fetch(f"/coins/list")

    def get_coin_id(self, name):
        for coin in self.coins:
            if coin['symbol'].lower() == name.lower():
                return coin['id']
        return None

    def get_price(self, coin):
        prices = self.fetch(f"/simple/price?ids={coin}&vs_currencies=USD")
        return prices[coin]['usd']
