from util import HTTPClient

class Bifrost(HTTPClient):
    def health(self):
        try:
            self.get("/p2pid", {"timeout": 2})
            return True
        except:
            return False
