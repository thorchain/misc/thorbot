import logging
import socket
from contextlib import closing

from thornode import THORNode
from tendermint import Tendermint
from midgard import Midgard
from bifrost import Bifrost

thor = THORNode()

def check_socket(host, port):
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
        sock.settimeout(2)
        if sock.connect_ex((host, port)) == 0:
            return True
        else:
            return False

def check_node(node):
    ip = node['ip_address']
    addr = node['node_address']

    thor = THORNode(f"http://{ip}:1317")
    tender = Tendermint(f"http://{ip}:27147")
    midgard = Midgard(f"http://{ip}:8080")
    bifrost = Bifrost(f"http://{ip}:6040")

    state = {}

    if thor.health() is False:
        logging.info(f"THORNode is down: {ip}")
    
    if tender.health() is False:
        logging.info(f"Tendermint is down: {ip}")
    
    if midgard.health() is False:
        logging.info(f"Midgard is down: {ip}")
    
    if bifrost.health() is False:
        logging.info(f"Bifrost is down: {ip}")


def mon_thornode_available():
    nodes = thor.get_nodes("Active")
    logging.info("got here")

    for node in nodes:
        check_node(node)

    return None
