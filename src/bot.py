# bot.py
import asyncio
import datetime
import logging
import os
import sys
import time
import traceback

import discord
from discord import Intents
from discord.ext import commands, tasks
from dotenv import load_dotenv

from cmds import *
from puck import Puck
from thornode import THORNode

# Init logging
logging.basicConfig(
    format="%(asctime)s | %(levelname).4s | %(message)s",
    level=os.environ.get("LOGLEVEL", "INFO"),
)

load_dotenv()
TOKEN = os.getenv("DISCORD_TOKEN")
CHECK_INTERVAL = os.getenv("CHECK_INTERVAL", default=86400)
PREFIX = os.getenv("PREFIX", default=">")
SAY_CHAN = os.getenv("SAY_CHAN", default=859431857837703230)

logging.info("Starting THORChain Bot...")

intents = Intents.default()
intents.message_content = True
bot = commands.Bot(
    command_prefix=PREFIX,
    description="THORChain bot that fetches network information",
    intents=intents,
)
puck = Puck(bot)
thor = THORNode()


@bot.event
async def on_ready():
    thor_bot_instance = THORBot(bot)
    await bot.add_cog(thor_bot_instance)
    thor_bot_instance.gitlab_buddy.start()


class THORBot(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.lock = asyncio.Lock()

    #################### Commands ####################

    @commands.command(help="health check")
    async def ping(self, ctx):
        start_time = time.time()
        message = await ctx.send("Testing Ping...")
        end_time = time.time()
        await message.edit(
            content=f"Pong! {round(self.bot.latency * 1000)}ms\nAPI: {round((end_time - start_time) * 1000)}ms"
        )

    @commands.command(help="get current rune price")
    @commands.cooldown(rate=1, per=1)
    async def price(self, ctx):
        await ctx.send(cmd_price.cmd(ctx))

    @commands.command(help="get current block height")
    @commands.cooldown(rate=1, per=1)
    async def height(self, ctx):
        await ctx.send(cmd_height.cmd(ctx))

    @commands.command(help="get the queue")
    @commands.cooldown(rate=1, per=1)
    async def queue(self, ctx):
        await ctx.send(embed=cmd_queue.cmd(ctx))

    @commands.command(help="get next churn")
    @commands.cooldown(rate=1, per=1)
    async def churn(self, ctx):
        await ctx.send(cmd_churn.cmd(ctx))

    @commands.command(help="get next pool churn")
    @commands.cooldown(rate=1, per=1)
    async def next_pool(self, ctx):
        await ctx.send(embed=cmd_next_pool.cmd(ctx))

    @commands.command(help=f"get node information (ex {PREFIX}node thor1xxxxx)")
    @commands.cooldown(rate=1, per=1)
    async def node(self, ctx, addr: str):
        await ctx.send(embed=cmd_node.cmd(ctx, addr))

    @commands.command(help=f"get node status (ex {PREFIX}node thor1xxxxx)", hidden=True)
    @commands.cooldown(rate=1, per=1)
    async def node_status(self, ctx, addr: str):
        node = self.monitor.get_node(addr)
        if node is None:
            await ctx.send("not found")
            return
        await ctx.send(embed=cmd_node_status.cmd(ctx, node))

    @commands.command(help="get network version")
    @commands.cooldown(rate=1, per=1)
    async def version(self, ctx):
        await ctx.send(embed=cmd_version.cmd(ctx))

    @commands.command(help="get pending outbound transactions")
    @commands.cooldown(rate=1, per=1)
    async def outbound(self, ctx):
        for embed in cmd_outbound.cmd(ctx):
            await ctx.send(embed=embed)

    @commands.command(help="get scheduled outbound transactions")
    @commands.cooldown(rate=1, per=1)
    async def scheduled(self, ctx):
        for embed in cmd_scheduled.cmd(ctx):
            await ctx.send(embed=embed)

    @commands.command(help="get solvency of asgard")
    @commands.cooldown(rate=1, per=1)
    async def solvency(self, ctx):
        for embed in cmd_solvency.cmd(ctx):
            await ctx.send(embed=embed)

    @commands.command(help="get pool information")
    @commands.cooldown(rate=1, per=1)
    async def pool(self, ctx, asset: str):
        await ctx.send(embed=cmd_pool.cmd(ctx, asset))

    @commands.command(help="get mimir settings")
    @commands.cooldown(rate=1, per=1)
    async def mimir(self, ctx):
        await ctx.send(embed=cmd_mimir.cmd(ctx))

    @commands.command(help="get node mimir votes (ex avb1)")
    @commands.cooldown(rate=1, per=1)
    async def node_mimir(self, ctx, addr: str):
        await ctx.send(embed=cmd_mimir_node.cmd(ctx, addr))

    @commands.command(help="get mimir votes")
    @commands.cooldown(rate=1, per=1)
    async def votes(self, ctx, addr: str):
        await ctx.send(embed=cmd_mimir_vote.cmd(ctx, addr))

    @commands.command(help="fetch who owns the puck")
    @commands.cooldown(rate=1, per=1)
    async def puck(self, ctx):
        if hasattr(ctx.channel, "recipient"):
            ctx.channel.recipient = ctx.message.author
        await puck.runner(True, ctx.channel)

    # used as a hidden testing command
    # example command that get user input via emojis
    @commands.command(help="test", hidden=True)
    async def test(self, ctx):
        message = await ctx.send(f"Are you sure?")
        await message.add_reaction("✅")
        await message.add_reaction("❌")
        check = (
            lambda r, u: u == ctx.author and str(r.emoji) in "✅❌"
        )  # r=reaction, u=user

        try:
            reaction, user = await self.bot.wait_for(
                "reaction_add", check=check, timeout=30
            )
        except asyncio.TimeoutError:
            await message.edit(content="Cancelled, timed out.")
            return

        if str(reaction.emoji) == "✅":
            await ctx.send(content=f"done.")
            return

        await ctx.send(content="Cancelled.")

    ##################################################

    ##################### Events #####################

    @commands.Cog.listener()
    async def on_ready(self):
        logging.info(f"Logged in as {self.bot.user} (ID: {self.bot.user.id})")
        self.gitlab_buddy.start()

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        # if command has local error handler, return
        if hasattr(ctx.command, "on_error"):
            return

        # get the original exception
        error = getattr(error, "original", error)

        if isinstance(error, commands.CommandNotFound):
            return

        if isinstance(error, commands.BotMissingPermissions):
            missing = [
                perm.replace("_", " ").replace("guild", "server").title()
                for perm in error.missing_perms
            ]
            if len(missing) > 2:
                fmt = "{}, and {}".format("**, **".join(missing[:-1]), missing[-1])
            else:
                fmt = " and ".join(missing)
            _message = "I need the **{}** permission(s) to run this command.".format(
                fmt
            )
            await ctx.send(_message)
            return

        if isinstance(error, commands.DisabledCommand):
            await ctx.send("This command has been disabled.")
            return

        if isinstance(error, commands.CommandOnCooldown):
            await ctx.send(
                "This command is on cooldown, please retry in {}s.".format(
                    math.ceil(error.retry_after)
                )
            )
            return

        if isinstance(error, commands.MissingPermissions):
            missing = [
                perm.replace("_", " ").replace("guild", "server").title()
                for perm in error.missing_perms
            ]
            if len(missing) > 2:
                fmt = "{}, and {}".format("**, **".join(missing[:-1]), missing[-1])
            else:
                fmt = " and ".join(missing)
            _message = "You need the **{}** permission(s) to use this command.".format(
                fmt
            )
            await ctx.send(_message)
            return

        if isinstance(error, commands.UserInputError):
            await ctx.send("Invalid input.")
            await self.send_command_help(ctx)
            return

        if isinstance(error, commands.NoPrivateMessage):
            try:
                await ctx.author.send("This command cannot be used in direct messages.")
            except discord.Forbidden:
                pass
            return

        if isinstance(error, commands.CheckFailure):
            await ctx.send("You do not have permission to use this command.")
            return

        # ignore all other exception types, but print them to stderr
        logging.error("Ignoring exception in command {}: {}".format(ctx.command, error))

        traceback.print_exception(
            type(error), error, error.__traceback__, file=sys.stdout
        )
        embed = discord.Embed(
            title="Error: {ctx.command}",
            description=error,
            colour=0xFF4136,
            timestamp=datetime.datetime.utcnow(),
        )
        await ctx.send(embed=embed, delete_after=10)

    #################################################

    ##################### Tasks #####################

    @tasks.loop(seconds=int(CHECK_INTERVAL))
    async def gitlab_buddy(self):
        await self.bot.wait_until_ready()
        async with self.lock:
            await puck.runner()

    #################################################


# bot.add_cog(THORBot(bot))
bot.run(TOKEN)
