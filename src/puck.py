import logging
import os
import traceback
from datetime import datetime

import discord
import gitlab
import pytz
from colorama import Fore, Style, init

init(autoreset=True)


class Puck:

    # discord channel ID
    THE_PUCK = int(os.getenv("THE_PUCK", 1134511392947437633))

    # Gitlab --> Discord ID lookup
    GITLAB_DISCORD_LOOKUP = {
        # "son-of-odin": "602442101803712552",
        "pluto-9r": "868185152281530438",
        "ursa9r": "902270076017868891",
        # "heimdallthor": "751679894789947482",
        "AsmundTHORSec": "872165894124609587",
        "akrokr": "872901672710590504",
        "the_eridanus": "867086416760537098",
        "Aquila_9R": "867086052619452437",
        "dixitaniket": "762238197492613151",
        "TxCorpi0x": "810855992489148437",
        # "fandral": "713197260040830986",
        "hildisviniottar": "831522955971264592",
        # "thorchain-admin": "645829022323966002",
        "Multipartite": "577161810914836480",
        "oleprohon": "816849636601888800",
        "thorchain-admin": "645829022323966002",
    }

    # Gitlab --> Timzezone lookup
    GITLAB_TZ_LOOKUP = {
        # "son-of-odin": "Japan",
        "pluto-9r": "EST",
        "ursa9r": "EST",
        # "heimdallthor": "Australia/Melbourne",
        "AsmundTHORSec": "US/Pacific",
        "akrokr": "US/Pacific",
        "the_eridanus": "MST",
        "Aquila_9R": "MST",
        # "fandral": "CET",
        "hildisviniottar": "Australia/Melbourne",
        # "thorchain-admin": "Australia/Melbourne",
    }

    # Gitlab Group --> members lookup
    devs = [
        # "son-of-odin",
        "pluto-9r",
        "dixitaniket",
        "TxCorpi0x",
        "Multipartite",
        # "heimdallthor",
        "the_eridanus",
        "Aquila_9R",
        # "thorchain-admin",
        # "fandral",
        "ursa9r",
    ]
    thorsec = ["AsmundTHORSec", "akrokr"]
    GITLAB_DISCORD_TEAM_LOOKUP = {
        "dev-team": devs,
        "DevTeam-Approval": devs,
        "devteam-mainnet": devs,
        "thorsec-develop": thorsec,
        "thorsec": thorsec,
        "Security": thorsec,
        "thorsec-mainnet": thorsec,
    }

    # Gitlab repos to scan
    GITLAB_REPOS = [
        {"project": "thorchain/thornode", "milestone-prefix": "Release-"},
        {"project": "thorchain/devops/node-launcher", "milestone-prefix": None},
    ]

    def __init__(self, bot):
        self.bot = bot
        self.say = self.bot.get_channel(self.THE_PUCK)
        logging.info(f"THE PUCK: {self.THE_PUCK}")

        GITLAB_TOKEN = os.getenv("GITLAB_TOKEN")
        self.DEBUG = os.getenv("DEBUG", "False").lower() in ("true", "1", "t")
        if GITLAB_TOKEN is None:
            logging.error("no gitlab token found (GITLAB_TOKEN)")
            return

        self.gl = gitlab.Gitlab(private_token=GITLAB_TOKEN)

    def is_timezone_eligible(self, username):
        return True
        if username not in self.GITLAB_TZ_LOOKUP:
            return False
        ts = datetime.now(pytz.timezone(self.GITLAB_TZ_LOOKUP[username]))
        hour = ts.hour
        day = ts.weekday()
        if day >= 5:
            return False  # is weekend
        if 9 > hour or hour >= 18:
            return False  # outside working hours
        return True

    def filter_on_timezone(self, usernames):
        return [u for u in usernames if self.is_timezone_eligible(u)]

    def is_any_timezone_eligible(self):
        return any([self.is_timezone_eligible(u) for u in self.GITLAB_TZ_LOOKUP])

    async def say_it(self, mr, embed, mention=[]):
        color = Fore.BLUE
        if embed.color == discord.Color.red():
            color = Fore.RED
        elif embed.color == discord.Color.orange():
            color = Fore.MAGENTA
        elif embed.color == discord.Color.gold():
            color = Fore.YELLOW
        elif embed.color == discord.Color.green():
            color = Fore.GREEN
        if type(mention) != list:
            mention = [mention]

        mention = list(set(mention))  # ensure is unique
        mention = [
            m for m in mention if m in self.GITLAB_DISCORD_LOOKUP
        ]  # disregard non-supported users

        # configured for DMs ONLY for a specific requesting user
        if self.say.type.name == "private" and self.say.recipient is not None:
            mention = [
                m
                for m in mention
                if int(self.GITLAB_DISCORD_LOOKUP[m]) == int(self.say.recipient.id)
            ]
        else:
            # exclude based on timezone
            mention = self.filter_on_timezone(mention)

        peeps = ", ".join([f"<@{self.GITLAB_DISCORD_LOOKUP[m]}>" for m in mention])
        if len(mention) == 0:
            print(color + ">> No mention to speak of")
            if not self.DEBUG:
                return

        print(
            color
            + f">>> {embed.title} {embed.description} ({', '.join(mention)}) {embed.url}"
        )
        print(Style.RESET_ALL)

        embed.set_footer(text=f"Branch: origin/{mr.source_branch}")

        if self.DEBUG:
            return
        await self.say.send(embed=embed)
        if self.say.type.name != "private":
            await self.say.send(f"cc {peeps}")

    async def has_conflicts(self, project, mr):
        # check for conflicts
        if mr.has_conflicts:
            embed = discord.Embed(
                title=f"{project.path_with_namespace} / {mr.title}",
                description=f"Needs to be rebased from {mr.target_branch}",
                url=mr.web_url,
                color=discord.Color.blue(),
            )
            await self.say_it(mr, embed, mr.author["username"])
            return True
        return False

    async def has_pipelines(self, project, mr):
        pipes = mr.pipelines.list(sort="desc", per_page=30)
        pipe = pipes[0]
        if pipe.status != "success":
            try:
                pipeline = project.pipelines.get(pipe.id)
                jobs = pipeline.jobs.list()
                jobs = sorted(jobs, key=lambda j: j.id, reverse=True)
                failed = [j for j in jobs if j.status == "failed"]
                if len(failed) > 0:
                    job = project.jobs.get(failed[0].id)
                    if job.stage == "smoke-test" and hasattr(job, "retry"):
                        job.retry()
                    embed = discord.Embed(
                        title=f"{project.path_with_namespace} / {mr.title}",
                        description=f"CI failure: {job.stage}",
                        url=mr.web_url,
                        color=discord.Color.red(),
                    )
                    await self.say_it(mr, embed, mr.author["username"])
            except Exception:
                traceback.print_exc()
                return False
        return pipe.status != "success"

    async def has_discussions(self, project, mr):
        result = False
        discussions = mr.discussions.list(per_page=100)
        for i, d in enumerate([d for d in discussions]):
            # skip resolved discussions
            resolved = [n["resolved"] for n in d.attributes["notes"] if "resolved" in n]
            if any(resolved):
                continue
            # get a list of people in the chat
            notes = [n for n in d.attributes["notes"] if n["resolvable"]]
            speakers = list(
                set([n["author"]["username"] for n in notes] + [mr.author["username"]])
            )
            if len(notes) == 0:
                continue
            note = notes[-1]
            # remove last commenter from list so they don't get notifications
            speakers = [s for s in speakers if s != note["author"]["username"]]
            if len(speakers) == 0:
                continue

            # alert each speaker of the discussion
            embed = discord.Embed(
                title=f"{project.path_with_namespace} / {mr.title}",
                description=note["body"],
                url=f"{mr.web_url}#note_{note['id']}",
                color=discord.Color.orange(),
            )
            embed.set_author(
                name=note["author"]["name"],
                url=note["author"]["web_url"],
                icon_url=note["author"]["avatar_url"],
            )
            await self.say_it(mr, embed, speakers)
            result = True
        return result

    async def ship_it(self, project, mr):
        embed = discord.Embed(
            title=f"{project.path_with_namespace} / {mr.title}",
            description="Ship it! ⛴",
            url=mr.web_url,
            color=discord.Color.green(),
        )
        await self.say_it(mr, embed, "pluto-9r")

    async def requires_approvals(self, project, mr):
        result = False
        names = []
        approvals = mr.approvals.get()
        approved = [x["user"]["username"] for x in approvals.approved_by]
        for a in approvals.approval_rules_left:
            print(a)
            if a["name"] in self.GITLAB_DISCORD_TEAM_LOOKUP:
                names += self.GITLAB_DISCORD_TEAM_LOOKUP[a["name"]]
            else:
                logging.error(f"bad gitlab group name {a['name']}")
                embed = discord.Embed(
                    description=f"bad gitlab group name {a['name']}",
                    color=discord.Color.red(),
                )
                await self.say_it(mr, embed, names)
        names = list(
            set([n for n in names if n != mr.author["username"] and n not in approved])
        )
        if len(names) > 0:
            result = True
            embed = discord.Embed(
                title=f"{project.path_with_namespace} / {mr.title}",
                description="awaiting review from you",
                url=mr.web_url,
                color=discord.Color.gold(),
            )
            await self.say_it(mr, embed, names)
        return result

    async def process_repo(self, repo, manual):
        project = self.gl.projects.get(repo["project"])

        milestones = []
        if "milestone-prefix" in repo and repo["milestone-prefix"] is not None:
            milestones = project.milestones.list(state="active")
            milestones = [
                m for m in milestones if m.title.startswith(repo["milestone-prefix"])
            ]
            milestones = sorted(milestones, key=lambda m: m.title)

        mrs = []
        if len(milestones) == 0:
            mrs += project.mergerequests.list(state="opened")
        else:
            # we only want to highlight the current milestone (ie the first
            # milestone that has open MRs)
            for stone in milestones:
                mrs += stone.merge_requests()
                mrs = [m for m in mrs if m.state == "opened"]  # the state
                if len(mrs) > 0:
                    break

        for mr in mrs:
            # ignore non-open MRs
            if mr.state != "opened":
                continue

            # ignore drafts
            if mr.draft:
                continue

            if mr.project_id != project.id:
                continue

            blocked = False

            if await self.has_conflicts(project, mr):
                blocked = True

            if await self.has_pipelines(project, mr):
                blocked = True

            if await self.has_discussions(project, mr):
                blocked = True

            # see if we're waiting on additional approvals
            if not blocked:
                if await self.requires_approvals(project, mr):
                    blocked = True

            # see if we can merge the MR
            if not blocked:
                await self.ship_it(project, mr)

    async def runner(self, manual=False, channel=None):
        self.manual = manual
        if channel is None:
            self.say = self.bot.get_channel(self.THE_PUCK)
        else:
            self.say = channel

        if manual is False and not self.is_any_timezone_eligible() and not self.DEBUG:
            print("no timezone eligable people")
            return

        for repo in self.GITLAB_REPOS:
            await self.process_repo(repo, manual)

        if manual is True:
            await self.say.send("done.")
