from datetime import datetime

from thornode import THORNode
from util import asset1e8

import discord

thor = THORNode()

# function to get unique values
def unique(list1):

    # initialize a null list
    unique_list = []

    # traverse for all elements
    for x in list1:
        # check if exists in unique_list or not
        if x not in unique_list:
            unique_list.append(x)
    # print list
    return unique_list

def cmd(ctx, addr):
    mimirs = thor.get_nodes_mimirs()

    addrs = unique([x['signer'] for x in mimirs if x['signer'].endswith(addr)])
    if len(addrs) > 1:
        return f"{addr} matches more than one address"
    if len(addrs) == 0:
        return f"{addr} matches no known addresses"

    mimirs = [x for x in mimirs if x['signer'] == addrs[0]]

    embed = discord.Embed(title="Node Mimir", description=addrs[0], colour=0x87CEEB, timestamp=datetime.utcnow())
    for m in mimirs:
        embed.add_field(name=m['key'], value=m['value'], inline=False)
    return embed
