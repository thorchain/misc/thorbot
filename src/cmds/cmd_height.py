from thornode import THORNode
from util import block_duration

thor = THORNode()

def cmd(ctx):
    height = thor.get_height()
    return f"Block Height is currently {height:,} (approx age: {block_duration(height)})"
