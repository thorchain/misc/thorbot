from datetime import datetime

from thornode import THORNode
from util import asset1e8

import discord

thor = THORNode()

def cmd(ctx, key):
    if key == "":
        return "no vote key given"

    key = key.upper()
    mimirs = thor.get_nodes_mimirs()
    mimirs = [x for x in mimirs if x['key'] == key]
    if len(mimirs) == 0:
        return "no votes found"

    nodes = thor.get_nodes()
    active = [x['node_address'] for x in nodes if x['status'] == "Active"]

    votes = {}
    voted = []
    for m in mimirs:
        if m['value'] not in votes:
            votes[m['value']] = 0
        if m['signer'] not in active:
            continue
        votes[m['value']] += 1
        voted.append(m['signer'])

    novotes = []
    for a in active:
        if a not in voted:
            novotes.append(a)

    embed = discord.Embed(title="Node Mimir Votes", description=f"{key}", colour=0x87CEEB, timestamp=datetime.utcnow())
    for k in sorted(votes):
        v = votes[k]
        embed.add_field(name=f"Cast: {k}", value=f"{v}/{len(active)} ({v/len(active)*100.0:,.2f}%)", inline=False)
    embed.add_field(name=f"Cast: no vote", value=f"{len(novotes)}/{len(active)} ({len(novotes)/len(active)*100.0:,.2f}%)", inline=False)
    novotes_condensed = [x[-4:] for x in novotes]
    embed.add_field(name=f"Not voted", value="\n".join(novotes_condensed), inline=False)
    return embed
