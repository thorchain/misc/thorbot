from datetime import datetime

from thornode import THORNode
from util import block_duration

import discord

thor = THORNode()

def cmd(ctx, node):

    embed = discord.Embed(title="Node Status", description=node.name, colour=0x87CEEB, timestamp=datetime.utcnow())
    for k, v in node.get_state().items():
        embed.add_field(name=k, value=v, inline=True)

    return embed
