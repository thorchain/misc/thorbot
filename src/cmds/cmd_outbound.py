from datetime import datetime

from thornode import THORNode
from util import block_duration

import discord

thor = THORNode()

def cmd(ctx):
    outbound = thor.get_outbound()

    embeds = []
    for o in outbound:
        if "to_address" not in o:
            continue
        embed = discord.Embed(title=o['to_address'], description=f"[{o['in_hash']}](https://viewblock.io/thorchain/tx/{o['in_hash']})", colour=0x87CEEB, timestamp=datetime.utcnow())
        embed.add_field(name="Coin", value=f"{int(o['coin']['amount'])/1e8:,.2f} {o['coin']['asset']}", inline=True)
        embed.add_field(name="Memo", value=o['memo'], inline=False)
        embeds.append(embed)
    return embeds
