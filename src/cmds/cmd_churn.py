import math
from thornode import THORNode
from util import block_duration

thor = THORNode()

def cmd(ctx):
    # TODO: approx which nodes will get churned in vs out
    height = thor.get_height()
    churn = thor.get_mimir("CHURNINTERVAL")
    asgards = thor.get_asgards()
    last_churn = int(asgards[0]['block_height'])
    next_churn = (last_churn + churn) - height
    if next_churn < 0:
        retries = math.floor(abs(next_churn) / 720)
        next_churn = (last_churn + churn + ((retries+1)*720)) - height
        return f"Next Churn is in {next_churn:,} blocks (approx: {block_duration(next_churn)}) (retries: {retries})"
    return f"Next Churn is in {next_churn:,} blocks (approx: {block_duration(next_churn)})"
