from datetime import datetime

from thornode import THORNode
from util import asset1e8

import discord

thor = THORNode()

def cmd(ctx):
    mimir = thor.get_mimirs()

    embed = discord.Embed(title="Mimir", description="Mimir settings", colour=0x87CEEB, timestamp=datetime.utcnow())
    for k,v in mimir.items():
        embed.add_field(name=k, value=v, inline=False)
    return embed
