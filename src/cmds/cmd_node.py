from datetime import datetime

from thornode import THORNode
from util import block_duration

import discord

thor = THORNode()

def cmd(ctx, addr):
    height = thor.get_height()
    nodes = thor.get_nodes()
    node = None
    for n in nodes:
        if n['node_address'].endswith(addr):
            node = n
            break
    
    if node is None:
        embed = discord.Embed(title="Node", description="Not found", colour=0x87CEEB, timestamp=datetime.utcnow())
        return embed

    embed = discord.Embed(title=node['node_address'], description="Node Information", colour=0x87CEEB, timestamp=datetime.utcnow())
    embed.add_field(name="Status", value=node['status'], inline=True)
    embed.add_field(name="Status Since", value=f"{node['status_since']:,} (approx {block_duration(height-node['status_since'])})", inline=False)
    embed.add_field(name="Bond", value=f"{int(node['bond']) / 1e8:,}", inline=True)
    embed.add_field(name="Reward", value=f"{int(node['current_award']) / 1e8:,}", inline=True)
    embed.add_field(name="Slash Points", value=f"{node['slash_points']:,}", inline=True)
    embed.add_field(name="Vault Membership", value=f"{len(node['signer_membership']):,}", inline=True)
    embed.add_field(name="Version", value=f"{node['version']}", inline=True)
    embed.add_field(name="Preflight", value=f"{node['preflight_status']['status']}: {node['preflight_status']['reason']}", inline=True)

    if 'jail' in node and 'release_height' in node['jail'] and height < node['jail']['release_height']:
        remaining = node['jail']['release_height'] - heigh
        embed.add_field(name="Jail", value=f"{node['jail']['reason']} (blocks remaining {remaining}, approx {block_duration(remaining)}", inline=True)
    return embed
