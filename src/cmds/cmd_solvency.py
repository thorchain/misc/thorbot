import logging
import importlib
from datetime import datetime

from thornode import THORNode
from util import HTTPClient

import discord
from brownie import Contract, network, web3


thor = THORNode()
network.connect("mainnet")


class Binance(HTTPClient):
    def __init__(self, chain=None):
        self.base_url = f"https://dex.binance.org/api/v1"

    def get_balances(self, address):
        raw = self.fetch(f"/account/{address}")
        return raw["balances"]

class Blockchair(HTTPClient):
    def __init__(self, chain=None):
        self.base_url = f"https://api.blockchair.com/{chain}"

    def get_balance(self, address):
        raw = self.fetch(f"/dashboards/address/{address}")
        return int(raw["data"][address.lower()]["address"]["balance"]) or 0

    def get_erc20_balance(self, address):
        raw = self.fetch(f"/dashboards/address/{address}?erc_20=true")
        return raw["data"][address.lower()]["layer_2"]["erc_20"]

def cmd(ctx):
    asgards = thor.get_asgards()

    # TODO: check yggdrasils
    # yggs = thor.get_yggdrasils()

    embeds = []
    for vault in asgards:
        embed = discord.Embed(title="THORChain Solvency", description=vault['pub_key'], colour=0x87CEEB, timestamp=datetime.utcnow())
        insolvent = False
        result = {}
        result = result | checkBTC(vault)
        result = result | checkBCH(vault)
        result = result | checkLTC(vault)
        result = result | checkBNB(vault)
        result = result | checkETH(vault)
        for k,v in result.items():
            if v < -0.00001: # only account for 4 decimal places, beyond that its sub pennies
                logging.info(f"Insolvency: {k}: {v:,.4f}")
                insolvent = True
                embed.add_field(name=k, value=f"{v:,.4f}", inline=False)
        if insolvent:
            embeds.append(embed)

    if len(embeds) == 0:
        embed = discord.Embed(title="THORChain Solvency", description="Displays any insolvency issues on the network", colour=0x87CEEB, timestamp=datetime.utcnow())
        embed.add_field(name="OK!", value="Network is solvent!", inline=True)
        embeds.append(embed)

    return embeds

def get_vault_address(vault, chain):
    for addr in vault["addresses"]:
        if addr["chain"].lower() == chain.lower():
            return addr["address"]
    return None

def get_vault_contract(vault, chain):
    for addr in vault["routers"]:
        if addr["chain"].lower() == chain.lower():
            return addr["router"]
    return None


def checkBTC(vault):
    return checkUTXO(vault, "bitcoin", "BTC")

def checkBCH(vault):
    return checkUTXO(vault, "bitcoin-cash", "BCH")

def checkLTC(vault):
    return checkUTXO(vault, "litecoin", "LTC")

def checkUTXO(vault, chain, apprev):
    chair = Blockchair(chain)
    asset = f"{apprev}.{apprev}"
    vault_address = get_vault_address(vault, apprev)
    vault_balance = 0
    for coin in vault["coins"]:
        if coin["asset"] == asset:
            vault_balance = int(coin["amount"])
            break
    wallet_balance = chair.get_balance(vault_address)

    return {asset: (wallet_balance - vault_balance) / 1e8}

def checkBNB(vault):
    binance = Binance()
    vault_address = get_vault_address(vault, "BNB")
    wallet_balances = binance.get_balances(vault_address)
    
    result = {}
    for coin in vault["coins"]:
        vault_balance = int(coin["amount"])
        decimals = 8
        if "decimals" in coin:
            decimals = coin["decimals"]
        vault_balance = vault_balance / 10**decimals
        if coin["asset"].startswith("BNB."):
            for bal in wallet_balances:
                if coin["asset"].split(".")[1] == bal["symbol"]:
                    result[coin["asset"]] = float(bal["free"]) - vault_balance

    return result

def checkETH(vault):
    chair = Blockchair("ethereum")
    vault_address = get_vault_address(vault, "ETH")
    contract_address = get_vault_contract(vault, "ETH")
    contract = Contract.from_explorer(contract_address)
    vault_balance = 0
    result = {}
    for coin in vault["coins"]:
        vault_balance = int(coin["amount"])
        decimals = 8
        if "decimals" in coin:
            decimals = coin["decimals"]
        vault_balance = vault_balance / 1e8
        if coin["asset"] == "ETH.ETH":
            wallet_balance = chair.get_balance(vault_address) / 1e18
            result[coin["asset"]] = (wallet_balance - vault_balance)
            continue
        if coin["asset"] == "ETH.RUNE-0X3155BA85D5F96B2D030A4966AF206230E46849CB":
            continue # skip eth.rune
        if coin["asset"].startswith("ETH."):
            token = coin["asset"].split("-")[-1].lower()
            wallet_balance = contract.vaultAllowance(vault_address, token)
            if decimals == 8:
                decimals = 18
            wallet_balance /= 10**decimals
            result[coin["asset"]] = wallet_balance - vault_balance

    return result
