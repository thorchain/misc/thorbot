from datetime import datetime

from thornode import THORNode
from midgard import Midgard
from util import asset1e8

import discord

midgard = Midgard()
thor = THORNode()

def cmd(ctx, asset):
    asset = asset.upper()
    pool = midgard.get_pool(asset)

    rune_price = thor.get_rune_price()

    embed = discord.Embed(title=asset, description=f"${float(pool['assetPriceUSD']):,.2f} ({pool['status']})", colour=0x87CEEB, timestamp=datetime.utcnow())
    embed.add_field(name="Depth", value=f"${float(pool['assetPriceUSD']) * (float(pool['assetDepth'])/1e8):,.2f}", inline=True)
    embed.add_field(name="24hr Volume", value=f"${float(pool['volume24h']) * rune_price / 1e8:,.0f}", inline=True)
    embed.add_field(name="APY", value=f"{float(pool['poolAPY']) * 100:,.2f}%", inline=True)
    return embed
