from thornode import THORNode

thor = THORNode()

def cmd(ctx):
    rune_price = thor.get_rune_price()
    return f'**Rune price**:\t${"{:,.2f}".format(rune_price)}'
