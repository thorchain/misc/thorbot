from datetime import datetime

from thornode import THORNode
from midgard import Midgard
from util import asset1e8, block_duration

import discord

midgard = Midgard()
thor = THORNode()

def cmd(ctx):
    blocks = thor.get_mimir("POOLCYCLE")
    min_depth = thor.get_mimir("MINRUNEPOOLDEPTH")
    height = thor.get_height()
    rune_price = thor.get_rune_price()

    pools = thor.get_pools("Staged")
    if len(pools) == 0:
        return "No pools to churn in"
    pool = {'balance_rune': 0}
    for p in pools:
        if int(pool['balance_rune']) < int(p['balance_rune']) and int(p['balance_rune']) > min_depth:
            pool = p

    if 'asset' not in pool:
        embed = discord.Embed(title=f"Next Pool", description="None", colour=0x87CEEB, timestamp=datetime.utcnow())
        return embed

    asset = pool['asset']

    pool = midgard.get_pool(asset)
    remain = blocks - (height % blocks)

    embed = discord.Embed(title=f"Next Pool ({asset})", description="Next pool to chrun into the network", colour=0x87CEEB, timestamp=datetime.utcnow())
    embed.add_field(name="Rune Depth", value=f"${float(rune_price) * (float(pool['runeDepth'])/1e8):,.0f}", inline=True)
    price = rune_price * (int(pool['runeDepth']) / 1e8) / (int(pool['assetDepth']) / 1e8)
    embed.add_field(name="Asset Price", value=f"${price:,.2f}", inline=True)
    embed.add_field(name="Remaining", value=block_duration(remain), inline=True)

    return embed
