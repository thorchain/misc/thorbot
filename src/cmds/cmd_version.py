from datetime import datetime

from thornode import THORNode
from util import block_duration

import discord

thor = THORNode()

def cmd(ctx):
    v = thor.get_version()
    nodes = thor.get_nodes("Active")

    versions = {}
    for node in nodes:
        version = node['version']
        if version not in versions:
            versions[version] = 0
        versions[version] += 1


    embed = discord.Embed(title="Network Version", description=f"Currently {v['current']}", colour=0x87CEEB, timestamp=datetime.utcnow())
    for k in sorted(versions):
        v = versions[k]
        embed.add_field(name=k, value=v, inline=True)
    return embed
