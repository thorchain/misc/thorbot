from datetime import datetime

from thornode import THORNode
from coingecko import CoinGecko
from util import block_duration

import discord

thor = THORNode()
gecko = CoinGecko()

def cmd(ctx):
    scheduled = thor.get_scheduled()
    height = thor.get_height()

    prices = {}

    done_hashes = []
    embeds = []
    for o in scheduled:
        if "to_address" not in o:
            continue
        txhash = o['in_hash']
        if txhash in done_hashes:
            continue
        done_hashes.append(txhash)
        details = thor.get_tx(txhash)
        memo = details['tx']['tx']['memo']
        outs = {}
        for out in details['actions']:
            coin = out['coin']
            if coin['asset'] not in outs:
                outs[coin['asset']] = 0
            outs[coin['asset']] += int(coin['amount'])

        embed = discord.Embed(title=o['to_address'], description=f"[{o['in_hash']}](https://viewblock.io/thorchain/tx/{o['in_hash']})", colour=0x87CEEB, timestamp=datetime.utcnow())
        for k,v in outs.items():
            price = 0
            if k in prices:
                price = prices[k]
            else:
                idx = gecko.get_coin_id(k.split('.')[1].split("-")[0])
                if idx is not None:
                    price = gecko.get_price(idx)
                    prices[k] = price
            value = (v/1e8) * price
            desc = f"{v/1e8:,.2f}"
            if value > 0:
                desc += f" ${value:,.2f}"
            embed.add_field(name=k, value=desc, inline=True)
        embed.add_field(name="Memo", value=memo, inline=False)
        diff = o['height'] - height
        if diff < 0:
            diff = 0
        delay = block_duration(o['height'] - height)
        embed.add_field(name="Scheduled in...", value=delay, inline=False)
        embeds.append(embed)

    if len(embeds) == 0:
        embed = discord.Embed(title="Scheduled Txns", description="Displays a list of scheduled txns", colour=0x87CEEB, timestamp=datetime.utcnow())
        embed.add_field(name="OK!", value="No scheduled txns", inline=True)
        embeds.append(embed)


    return embeds
