from datetime import datetime

from thornode import THORNode

import discord

thor = THORNode()

def cmd(ctx):
    q = thor.get_queue()
    rune_price = thor.get_rune_price()

    embed = discord.Embed(title="Queue Count", description="Network Queue Depth", colour=0x87CEEB, timestamp=datetime.utcnow())
    embed.add_field(name="Outbound Txns", value=q['outbound'], inline=False)
    embed.add_field(name="Internal Txns", value=q['internal'], inline=False)

    value = int(q['scheduled_outbound_value']) / 1e8 * rune_price
    embed.add_field(name="Scheduled Txn Value", value=f"${value:,.2f}", inline=False)
    return embed
