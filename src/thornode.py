import logging

from util import HTTPClient, isMainnet


class THORNode(HTTPClient):
    def __init__(self, base_url=None):
        if base_url is None:
            if isMainnet():
                self.base_url = "https://thornode.ninerealms.com"
            else:
                self.base_url = "https://stagenet-thornode.ninerealms.com"
        else:
            self.base_url = base_url

    def health(self):
        try:
            self.get("/thorchain/ping", {"timeout": 1})
            return True
        except:
            return False

    def get_height(self):
        resp = self.fetch("/thorchain/lastblock")
        return resp[0]["thorchain"]

    def get_node(self, node):
        return self.fetch(f"/thorchain/node/{node}")

    def get_nodes(self, status=None):
        resp = self.fetch(f"/thorchain/nodes")
        if status is None:
            return resp
        return [n for n in resp if n["status"] == status]

    def get_pool(self, asset):
        return self.fetch(f"/thorchain/pool/{asset}")

    def get_pools(self, status=None):
        resp = self.fetch(f"/thorchain/pools")
        if status is None:
            return resp
        return [p for p in resp if p["status"] == status]

    def get_tx(self, txhash):
        return self.fetch(f"/thorchain/tx/{txhash}/signers")

    def get_constant(self, key):
        resp = self.fetch(f"/thorchain/constants")
        if key in resp["int_64_values"]:
            return resp["int_64_values"][key]
        return None

    def get_mimir(self, key):
        resp = self.fetch(f"/thorchain/mimir")
        return resp.get(key.upper())

    def get_mimirs(self):
        resp = self.fetch(f"/thorchain/mimir")
        result = {}
        for k, v in resp.items():
            k = k.lstrip("mimir//")
            result[k] = v
        return result

    def get_nodes_mimirs(self):
        resp = self.fetch(f"/thorchain/mimir/nodes_all")
        return resp["mimirs"]

    def get_asgards(self):
        return self.fetch(f"/thorchain/vaults/asgard")

    def get_queue(self):
        return self.fetch(f"/thorchain/queue")

    def get_version(self):
        return self.fetch(f"/thorchain/version")

    def get_outbound(self):
        return self.fetch("/thorchain/queue/outbound")

    def get_scheduled(self):
        return self.fetch("/thorchain/queue/scheduled")

    def get_lps(self, asset):
        return self.fetch(f"/thorchain/pool/{asset}/liquidity_providers")

    def get_lp(self, asset, addr):
        return self.fetch(f"/thorchain/pool/{asset}/liquidity_provider/{addr}")

    def get_rune_price(self):
        pool = self.fetch("/thorchain/pool/BNB.BUSD-BD1")
        return round(float(pool["balance_asset"]) / float(pool["balance_rune"]), 2)

    def get_asgards(self):
        return self.fetch("/thorchain/vaults/asgard")

    def get_yggdrasils(self):
        return self.fetch("/thorchain/vaults/yggdrasil")
