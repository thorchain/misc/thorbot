from util import HTTPClient, isMainnet

class Tendermint(HTTPClient):
    def __init__(self, base_url=None):
        if base_url is None:
            if isMainnet():
                self.base_url = "https://rpc.thorchain.info"
            else:
                self.base_url = "https://testnet.rpc.thorchain.info"
        else:
            self.base_url = base_url

    def health(self):
        try:
            self.get("/health", {"timeout": 1})
            return True
        except:
            return False

    def get_height(self):
        resp = self.fetch("/block")
        return int(resp['result']['block']['header']['height'])
