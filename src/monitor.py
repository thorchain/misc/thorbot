import logging
import socket
from contextlib import closing

from thornode import THORNode
from tendermint import Tendermint
from midgard import Midgard
from bifrost import Bifrost

thor = THORNode()

NODE_CHECK_INTERVAL=30 # in blocks

def check_socket(host, port):
    ''' check if port is open '''
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
        sock.settimeout(2)
        if sock.connect_ex((host, port)) == 0:
            return True
        else:
            return False

def most_frequent(List):
    ''' most frequent item in list '''
    counter = 0
    num = List[0]
    for i in List:
        curr_frequency = List.count(i)
        if(curr_frequency> counter):
            counter = curr_frequency
            num = i
    return num


class Node:
    def __init__(self, node):
        self.name = node['node_address']
        self.ip = node['ip_address']
        self.status = node['status']
        self.reset()
        self.last_check = 0

    def set_status(self, status):
        self.status = status

    def set_state(self, k, v):
        if not v:
            logging.info(f"{self.name}: {k} is {v}")
        # maintain last five historical checks
        if k not in self.state:
            self.state[k] = []
        self.state[k].append(v)
        self.state[k] = self.state[k][:5]

    def get_state(self):
        result = {}
        for k, v in self.state.items():
            result[k] = most_frequent(v)
        return result

    def reset(self):
        self.state = {}

    def check_node(self):
        thor = THORNode(f"http://{self.ip}:1317")
        tender = Tendermint(f"http://{self.ip}:27147")
        midgard = Midgard(f"http://{self.ip}:8080")
        bifrost = Bifrost(f"http://{self.ip}:6040")

        height = thor.get_height()
        if height - self.last_check <= NODE_CHECK_INTERVAL:
            return 

        logging.info(f"Checking node.... {self.name}")
        self.last_check = height

        self.set_state("thornode_up", thor.health())
        self.set_state("tendermint_up", tender.health())
        self.set_state("midgard_up", midgard.health())
        self.set_state("bifrost_up", bifrost.health())

class Nodes:
    def __init__(self):
        self.thor = THORNode()
        self.nodes = []

    def has_node(self, name):
        for node in self.nodes:
            if name == node.name:
                return True
        return False

    def get_node(self, name):
        for node in self.nodes:
            if name == node.name:
                return node
        return None

    def update_set(self):
        logging.info("Update node set...")
        nodes = self.thor.get_nodes()

        for node in nodes:
            if node['status'] != 'Active':
                continue
            if not self.has_node(node['node_address']):
                self.nodes.append(Node(node))

        # update node list
        for n1 in self.nodes:
            for n2 in nodes:
                if n1.name == n2['node_address']:
                    if n1.status != n2['status']:
                        n1.reset()
                    n1.set_status(n2['status'])


    def check(self):
        for node in self.nodes:
            node.check_node()
            

def mon_thornode_available():
    nodes = thor.get_nodes("Active")
    logging.info("got here")

    for node in nodes:
        check_node(node)

    return None
